<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class WC_Correios_Product_Shipping_Simulator {

	/**
	 * Simulator is activated.
	 *
	 * @var bool
	 */
	private static $activated = false;

	/**
	 * Shipping simulator actions.
	 */
	public function __construct() {
		add_action( 'wp_enqueue_scripts', array( $this, 'scritps' ) );
		add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'simulator' ), 45 );
	}

	/**
	 * Shipping simulator scripts.
	 *
	 * @return void
	 */
	public function scritps() {
		if ( is_product() ) {
			$options         = get_option( 'woocommerce_correios_settings' );
			self::$activated = isset( $options['simulator'] ) && 'yes' == $options['simulator'] && 'yes' == $options['enabled'];

			if ( self::$activated ) {
				$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

				wp_enqueue_style( 'woocommerce-correios-simulator', plugins_url( 'assets/css/simulator' . $suffix . '.css', plugin_dir_path( __FILE__ ) ), array(), WC_Correios::VERSION, 'all' );
				wp_enqueue_script( 'woocommerce-correios-simulator', plugins_url( 'assets/js/simulator' . $suffix . '.js', plugin_dir_path( __FILE__ ) ), array( 'jquery' ), WC_Correios::VERSION, true );
				wp_localize_script(
					'woocommerce-correios-simulator',
					'woocommerce_correios_simulator',
					array(
						'ajax_url'      => admin_url( 'admin-ajax.php' ),
						'error_message' => __( 'It was not possible to simulate the shipping, please try adding the product to cart and proceed to try to get the value.', 'woocommerce-correios' )
					)
				);
			}
		}
	}

	/**
	 * Display the simulator.
	 *
	 * @return string Simulator HTML.
	 */
	public static function simulator() {
		global $product;

		if ( ! is_product() || ! self::$activated ) {
			return;
		}

		if ( $product->needs_shipping() && $product->is_in_stock() && in_array( $product->product_type, array( 'simple', 'variable' ) ) ) {
			$options = get_option( 'woocommerce_correios_settings' );
			if ( 'variable' == $product->product_type ) {
				$style = 'display: none';
				$ids   = array();

				foreach ( $product->get_available_variations() as $variation ) {
					$_variation = get_product( $variation['variation_id'] );
					$ids[] = ( $_variation->needs_shipping() ) ? $_variation->variation_id : '';
				}

				$ids = implode( ',', array_filter( $ids ) );
			} else {
				$style = '';
				$ids   = $product->id;
			}

			if ( isset( $options['display_date'] ) && 'yes' == $options['display_date'] ) {
				$title       = __( 'Shipping and delivery time', 'woocommerce-correios' );
				$description = __( 'Calculate the shipping and delivery time estimated to your region.', 'woocommerce-correios' );
			} else {
				$title       = __( 'Shipping', 'woocommerce-correios' );
				$description = __( 'Calculate shipping estimated to your region.', 'woocommerce-correios' );
			}

			wc_get_template( 'single-product/correios-simulator.php', array(
				'product'     => $product,
				'style'       => $style,
				'ids'         => $ids,
				'title'       => $title,
				'description' => $description,
			), '', WC_Correios::get_templates_path() );
		}
	}

	/**
	 * Get the price.
	 *
	 * @param  int    $value Shipping price.
	 *
	 * @return string        Formated shipping price.
	 */
	protected static function get_price( $value ) {
		if ( 0 == $value ) {
			return __( '(Free)', 'woocommerce-correios' );
		}

		return sanitize_text_field( wc_price( $value ) );
	}

	/**
	 * Simulator ajax response.
	 *
	 * @return string
	 */
	public static function ajax_simulator() {

		// Validate the data.
		if ( ! isset( $_GET['product_id'] ) || empty( $_GET['product_id'] ) ) {
			wp_send_json( array( 'error' => __( 'Error to identify the product.', 'woocommerce-correios' ), 'rates' => '' ) );
		}

		if ( ! isset( $_GET['zipcode'] ) || empty( $_GET['zipcode'] ) ) {
			wp_send_json( array( 'error' => __( 'Please enter with your zipcode.', 'woocommerce-correios' ), 'rates' => '' ) );
		}

		// Get the product data.
		$id         = ( isset( $_GET['variation_id'] ) && ! empty( $_GET['variation_id'] ) ) ? $_GET['variation_id'] : $_GET['product_id'];
		$product_id = absint( $id );
		$product    = get_product( $product_id );
		$quantity   = ( isset( $_GET['quantity'] ) && ! empty( $_GET['quantity'] ) ) ? $_GET['quantity'] : 1;

		// Test with the product exist.
		if ( ! $product ) {
			wp_send_json( array( 'error' => __( 'Invalid product!', 'woocommerce-correios' ), 'rates' => '' ) );
		}

		$content = array();
		if ( $product->product_type == 'variation' ) {
			$content['variation_id'] = $id;
			$content['variation'] = $product->get_variation_attributes();
		}
		$content['product_id'] = $product->post->ID;
		$content['quantity'] = $quantity;
		$content['data'] = $product;

		$package = array(
			'contents'        => array(
				$id => $content
			),
			'contents_cost'   => $quantity * $product->get_price(),
			'destination'     => array(
				'country'  => 'BR',
				'postcode' => $_GET['zipcode']
			)
		);

		$shipping = WC()->shipping->calculate_shipping_for_package( $package );

		// Send shipping rates.
		if ( isset( $shipping['rates'] ) && ! empty( $shipping['rates'] ) ) {
			// Format the cost.
			$rates = array();
			foreach ( $shipping['rates'] as $rate ) {
				$rates[] = array(
					'id'    => $rate->id,
					'label' => $rate->label,
					'cost'  => self::get_price( $rate->cost )
				);
			}
			wp_send_json( array( 'error' => '', 'rates' => $rates ) );
		}

		// Error.
		wp_send_json( array( 'error' => __( 'It was not possible to simulate the shipping, please try adding the product to cart and proceed to try to get the value.', 'woocommerce-correios' ), 'rates' => '' ) );
	}
}

new WC_Correios_Product_Shipping_Simulator();
