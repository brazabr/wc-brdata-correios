<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * WC_Correios_Shipping class.
 */
class WC_Correios_Shipping extends WC_Shipping_Method {

	/**
	 * Initialize the Correios shipping method.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->id                 = 'correios';
		$this->method_title       = __( 'Correios', 'woocommerce-correios' );
		$this->method_description = __( 'Correios is a brazilian delivery method.', 'woocommerce-correios' );

		// Load the form fields.
		$this->init_form_fields();

		// Load the settings.
		$this->init_settings();

		// Define user set variables.
		$this->enabled            = $this->get_option( 'enabled' );
		$this->title              = $this->get_option( 'title' );
		$this->declare_value      = $this->get_option( 'declare_value' );
		$this->display_date       = $this->get_option( 'display_date' );
		$this->additional_time    = $this->get_option( 'additional_time' );
		$this->fee                = $this->get_option( 'fee' );
		$this->zip_origin         = $this->get_option( 'zip_origin' );
		$this->simulator          = $this->get_option( 'simulator', 'no' );
		$this->tracking_history   = $this->get_option( 'tracking_history', 'no' );
		$this->corporate_service  = $this->get_option( 'corporate_service' );
		$this->login              = $this->get_option( 'login' );
		$this->password           = $this->get_option( 'password' );
		$this->service_regletter  = $this->get_option( 'service_regletter' );
		$this->regletter_table    = $this->get_option( 'regletter_table', 'commercial' );
		$this->regletter_column   = $this->get_option( 'regletter_column', 'regletter' );
		$this->service_pac        = $this->get_option( 'service_pac' );
		$this->service_paclarge   = $this->get_option( 'service_paclarge' );
		$this->service_sedex      = $this->get_option( 'service_sedex' );
		$this->service_sedex_10   = $this->get_option( 'service_sedex_10' );
		$this->service_sedex_hoje = $this->get_option( 'service_sedex_hoje' );
		$this->service_esedex     = $this->get_option( 'service_esedex' );
		$this->minimum_height     = $this->get_option( 'minimum_height' );
		$this->minimum_width      = $this->get_option( 'minimum_width' );
		$this->minimum_length     = $this->get_option( 'minimum_length' );
		$this->multiorigin        = $this->get_option( 'multiorigin' );
		$this->multiorigin_order  = $this->get_option( 'multiorigin_order', 'billing' );
		$this->debug              = $this->get_option( 'debug' );

		// Method variables.
		$this->availability       = 'specific';
		$this->countries          = array( 'BR' );

		// Actions.
		add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );

		// Active logs.
		if ( 'yes' == $this->debug ) {
			$this->log = new WC_Logger();
		}
	}

	/**
	 * Get log.
	 *
	 * @return string
	 */
	protected function get_log_view() {
		if ( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '2.2', '>=' ) ) {
			return '<a href="' . esc_url( admin_url( 'admin.php?page=wc-status&tab=logs&log_file=' . esc_attr( $this->id ) . '-' . sanitize_file_name( wp_hash( $this->id ) ) . '.log' ) ) . '">' . __( 'System Status &gt; Logs', 'woocommerce-correios' ) . '</a>';
		}

		return '<code>woocommerce/logs/' . esc_attr( $this->id ) . '-' . sanitize_file_name( wp_hash( $this->id ) ) . '.txt</code>';
	}

	/**
	 * Admin options fields.
	 *
	 * @return void
	 */
	public function init_form_fields() {
		$this->form_fields = array(
			'enabled' => array(
				'title'            => __( 'Enable/Disable', 'woocommerce-correios' ),
				'type'             => 'checkbox',
				'label'            => __( 'Enable this shipping method', 'woocommerce-correios' ),
				'default'          => 'no'
			),
			'title' => array(
				'title'            => __( 'Title', 'woocommerce-correios' ),
				'type'             => 'text',
				'description'      => __( 'This controls the title which the user sees during checkout.', 'woocommerce-correios' ),
				'desc_tip'         => true,
				'default'          => __( 'Correios', 'woocommerce-correios' )
			),
			'zip_origin' => array(
				'title'            => __( 'Origin Zip Code', 'woocommerce-correios' ),
				'type'             => 'text',
				'description'      => __( 'Zip Code from where the requests are sent.', 'woocommerce-correios' ),
				'desc_tip'         => true
			),
			'declare_value' => array(
				'title'            => __( 'Declare value', 'woocommerce-correios' ),
				'type'             => 'select',
				'default'          => 'none',
				'options'          => array(
					'declare'      => __( 'Declare', 'woocommerce-correios' ),
					'none'         => __( 'None', 'woocommerce-correios' )
				),
			),
			'display_date' => array(
				'title'            => __( 'Estimated delivery', 'woocommerce-correios' ),
				'type'             => 'checkbox',
				'label'            => __( 'Enable', 'woocommerce-correios' ),
				'description'      => __( 'Display date of estimated delivery.', 'woocommerce-correios' ),
				'desc_tip'         => true,
				'default'          => 'no'
			),
			'additional_time' => array(
				'title'            => __( 'Additional days', 'woocommerce-correios' ),
				'type'             => 'text',
				'description'      => __( 'Additional days to the estimated delivery.', 'woocommerce-correios' ),
				'desc_tip'         => true,
				'default'          => '0',
				'placeholder'      => '0'
			),
			'fee' => array(
				'title'            => __( 'Handling Fee', 'woocommerce-correios' ),
				'type'             => 'text',
				'description'      => __( 'Enter an amount, e.g. 2.50, or a percentage, e.g. 5%. Leave blank to disable.', 'woocommerce-correios' ),
				'desc_tip'         => true,
				'placeholder'      => '0.00'
			),
			'simulator' => array(
				'title'            => __( 'Simulator', 'woocommerce-correios' ),
				'type'             => 'checkbox',
				'label'            => __( 'Enable product shipping simulator', 'woocommerce-correios' ),
				'description'      => __( 'Displays a shipping simulator in the product page.', 'woocommerce-correios' ),
				'default'          => 'no'
			),
			'tracking_history' => array(
				'title'            => __( 'Tracking History Table', 'woocommerce-correios' ),
				'type'             => 'checkbox',
				'label'            => __( 'Enable Tracking History Table in view order page on frontend', 'woocommerce-correios' ),
				'description'      => __( 'Displays a table with the tracking history in My Account > View Order page.', 'woocommerce-correios' ),
				'default'          => 'no'
			),
			'services' => array(
				'title'            => __( 'Correios Services', 'woocommerce-correios' ),
				'type'             => 'title'
			),
			'corporate_service' => array(
				'title'            => __( 'Corporate Service', 'woocommerce-correios' ),
				'type'             => 'select',
				'description'      => __( 'Choose between conventional or corporate service.', 'woocommerce-correios' ),
				'desc_tip'         => true,
				'default'          => 'conventional',
				'options'          => array(
					'conventional' => __( 'Conventional', 'woocommerce-correios' ),
					'corporate'    => __( 'Corporate', 'woocommerce-correios' )
				),
			),
			'login' => array(
				'title'            => __( 'Administrative Code', 'woocommerce-correios' ),
				'type'             => 'text',
				'description'      => __( 'Your Correios login.', 'woocommerce-correios' ),
				'desc_tip'         => true
			),
			'password' => array(
				'title'            => __( 'Administrative Password', 'woocommerce-correios' ),
				'type'             => 'password',
				'description'      => __( 'Your Correios password.', 'woocommerce-correios' ),
				'desc_tip'         => true
			),
			'service_regletter' => array(
				'title'            => __( 'Registered Letter', 'woocommerce-correios' ),
				'type'             => 'checkbox',
				'label'            => __( 'Enable', 'woocommerce-correios' ),
				'description'      => __( 'Shipping via Registered Letter.', 'woocommerce-correios' ),
				'desc_tip'         => true,
				'default'          => 'no'
			),
			'regletter_table' => array(
				'title'            => __( 'Registered Letter Price Table', 'woocommerce-correios' ),
				'type'             => 'select',
				'class'            => 'regletter_option',
				'default'          => 'commercial',
				'options'          => array(
					'commercial'     => __( 'Commercial Letter', 'woocommerce-correios' ),
					'non_commercial' => __( 'Non-commercial Letter', 'woocommerce-correios' )
				),
			),
			'regletter_column' => array(
				'title'            => __( 'Registered Letter Extra Services', 'woocommerce-correios' ),
				'type'             => 'select',
				'class'            => 'regletter_option',
				'default'          => 'regletter',
				'options'          => array(
					'regletter'       => __( 'None', 'woocommerce-correios' ),
					'regletter_ar'    => __( 'Receipt Notice', 'woocommerce-correios' ),
		  		'regletter_mp'    => __( 'Own Hand', 'woocommerce-correios' ),
    			'regletter_ar_mp' => __( 'Receipt Notice + Own Hand', 'woocommerce-correios' )
				),
			),
			'service_pac' => array(
				'title'            => __( 'PAC', 'woocommerce-correios' ),
				'type'             => 'checkbox',
				'label'            => __( 'Enable', 'woocommerce-correios' ),
				'description'      => __( 'Shipping via PAC.', 'woocommerce-correios' ),
				'desc_tip'         => true,
				'default'          => 'no'
			),
			'service_paclarge' => array(
				'title'            => __( 'PAC Large Format', 'woocommerce-correios' ),
				'type'             => 'checkbox',
				'label'            => __( 'Enable', 'woocommerce-correios' ),
				'description'      => __( 'Shipping via PAC Large Format.', 'woocommerce-correios' ),
				'desc_tip'         => true,
				'default'          => 'no'
			),
			'service_sedex' => array(
				'title'            => __( 'SEDEX', 'woocommerce-correios' ),
				'type'             => 'checkbox',
				'label'            => __( 'Enable', 'woocommerce-correios' ),
				'description'      => __( 'Shipping via SEDEX.', 'woocommerce-correios' ),
				'desc_tip'         => true,
				'default'          => 'no'
			),
			'service_sedex_10' => array(
				'title'            => __( 'SEDEX 10', 'woocommerce-correios' ),
				'type'             => 'checkbox',
				'label'            => __( 'Enable', 'woocommerce-correios' ),
				'description'      => __( 'Shipping via SEDEX 10.', 'woocommerce-correios' ),
				'desc_tip'         => true,
				'default'          => 'no'
			),
			'service_sedex_hoje' => array(
				'title'            => __( 'SEDEX Hoje', 'woocommerce-correios' ),
				'type'             => 'checkbox',
				'label'            => __( 'Enable', 'woocommerce-correios' ),
				'description'      => __( 'Shipping via SEDEX Hoje.', 'woocommerce-correios' ),
				'desc_tip'         => true,
				'default'          => 'no'
			),
			'service_esedex' => array(
				'title'            => __( 'e-SEDEX', 'woocommerce-correios' ),
				'type'             => 'checkbox',
				'label'            => __( 'Enable', 'woocommerce-correios' ),
				'description'      => __( 'Shipping via e-SEDEX.', 'woocommerce-correios' ),
				'desc_tip'         => true,
				'default'          => 'no'
			),
			'package_standard' => array(
				'title'            => __( 'Package Standard', 'woocommerce-correios' ),
				'type'             => 'title',
				'description'      => __( 'Sets a minimum measure for the package.', 'woocommerce-correios' ),
				'desc_tip'         => true,
			),
			'minimum_height' => array(
				'title'            => __( 'Minimum Height', 'woocommerce-correios' ),
				'type'             => 'text',
				'description'      => __( 'Minimum height of the package. Correios needs at least 2 cm.', 'woocommerce-correios' ),
				'desc_tip'         => true,
				'default'          => '2'
			),
			'minimum_width' => array(
				'title'            => __( 'Minimum Width', 'woocommerce-correios' ),
				'type'             => 'text',
				'description'      => __( 'Minimum width of the package. Correios needs at least 11 cm.', 'woocommerce-correios' ),
				'desc_tip'         => true,
				'default'          => '11'
			),
			'minimum_length' => array(
				'title'            => __( 'Minimum Length', 'woocommerce-correios' ),
				'type'             => 'text',
				'description'      => __( 'Minimum length of the package. Correios needs at least 16 cm.', 'woocommerce-correios' ),
				'desc_tip'         => true,
				'default'          => '16'
			),
			'vendors' => array(
				'title'            => __( 'Multiple Vendors', 'woocommerce-correios' ),
				'type'             => 'title'
			),
			'multiorigin' => array(
				'title'            => __( 'Multiple Origins', 'woocommerce-correios' ),
				'type'             => 'checkbox',
				'label'            => __( 'Calculate shipping from multiple origins', 'woocommerce-correios' ),
				'default'          => 'no',
				'description'      => __( 'When using plugins like WC Vendors, Product Vendors or Dokan, enable this setting if you want to use calculate multiple shipments from different origins.', 'woocommerce-correios' )
			),
			'multiorigin_order' => array(
				'title'            => __( 'Postcode Order', 'woocommerce-correios' ),
				'type'             => 'select',
				'class'            => 'multiorigin',
				'default'          => 'billing',
				'options'          => array(
					'billing'  => __( 'Use vendor billing postcode, then shipping postcode', 'woocommerce-correios' ),
					'delivery' => __( 'Use vendor shipping postcode, then billing postcode', 'woocommerce-correios' ),
				),
				'description'      => __( 'The plugin will follow the above order to try to determine the origin postcode. If it fails, it will use the store default origin postcode.', 'woocommerce-correios' )
			),
			'testing' => array(
				'title'            => __( 'Testing', 'woocommerce-correios' ),
				'type'             => 'title'
			),
			'debug' => array(
				'title'            => __( 'Debug Log', 'woocommerce-correios' ),
				'type'             => 'checkbox',
				'label'            => __( 'Enable logging', 'woocommerce-correios' ),
				'default'          => 'no',
				'description'      => sprintf( __( 'Log Correios events, such as WebServices requests, inside %s.', 'woocommerce-correios' ), $this->get_log_view() )
			)
		);
	}

	/**
	 * Correios options page.
	 *
	 * @return void
	 */
	public function admin_options() {
		$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

		// Call the admin scripts.
		wp_enqueue_script( 'wc-correios', plugins_url( 'assets/js/admin' . $suffix . '.js', plugin_dir_path( __FILE__ ) ), array( 'jquery' ), '', true );

		include 'views/html-admin-page.php';
	}

	/**
	 * Gets the services IDs.
	 *
	 * @return array
	 */
	public function correios_services() {
		$services = array();

		$services['Registered Letter'] = ( 'yes' == $this->service_regletter )  ? '10014' : '';
		$services['PAC']               = ( 'yes' == $this->service_pac )        ? '41106' : '';
		$services['SEDEX']             = ( 'yes' == $this->service_sedex )      ? '40010' : '';
		$services['SEDEX 10']          = ( 'yes' == $this->service_sedex_10 )   ? '40215' : '';
		$services['SEDEX Hoje']        = ( 'yes' == $this->service_sedex_hoje ) ? '40290' : '';

		if ( 'corporate' == $this->corporate_service ) {
			$services['PAC']              = ( 'yes' == $this->service_pac )      ? '41068' : '';
			$services['PAC Large Format'] = ( 'yes' == $this->service_paclarge ) ? '41300' : '';
			$services['SEDEX']            = ( 'yes' == $this->service_sedex )    ? '40096' : '';
			$services['e-SEDEX']          = ( 'yes' == $this->service_esedex )   ? '81019' : '';
		}

		return array_filter( $services );
	}

	/**
	 * Gets the price of shipping.
	 *
	 * @param  array $package Order package.
	 *
	 * @return array          Correios Quotes.
	 */
	protected function correios_calculate( $package, $zip_origin = '' ) {
		$services = array_values( $this->correios_services() );
		$connect  = new WC_Correios_Connect;
		$connect->set_services( $services );
		$_package = $connect->set_package( $package );
		$_package->set_minimum_height( $this->minimum_height );
		$_package->set_minimum_width( $this->minimum_width );
		$_package->set_minimum_length( $this->minimum_length );
		$connect->set_zip_origin( $zip_origin );
		$connect->set_zip_destination( $package['destination']['postcode'] );
  	$connect->set_regletter_table( $this->regletter_table );
		$connect->set_regletter_column( $this->regletter_column );
		$connect->set_debug( $this->debug );
		if ( 'declare' == $this->declare_value ) {
			$declared_value = WC()->cart->cart_contents_total;
			$connect->set_declared_value( $declared_value );
		}

		if ( 'corporate' == $this->corporate_service ) {
			$connect->set_login( $this->login );
			$connect->set_password( $this->password );
		}

		$shipping = $connect->get_shipping();

		if ( ! empty( $shipping ) ) {
			return $shipping;
		} else {
			// Cart only with virtual products.
			if ( 'yes' == $this->debug ) {
				$this->log->add( 'correios', 'Cart only with virtual products.' );
			}

			return array();
		}
	}

	/**
	 * Calculates the shipping rate.
	 *
	 * @param array $package Order package.
	 *
	 * @return void
	 */
	public function calculate_shipping( $package = array() ) {
		$rates           = array();
		$errors          = array();

		if ( 'yes' == $this->multiorigin ) {
			$correios_packages = array();

			foreach ( $package['contents'] as $item_id => $item ) {
		    if ( isset( $item['data']->post->post_author ) ) {
		      $post_author = $item['data']->post->post_author;
		    } else {
		      $post_author = '-1';
		    }
				$post_author = apply_filters( 'woocommerce_correios_product_author', $post_author, $item );

				if ( !isset( $correios_packages[$post_author] ) ) {
					$correios_packages[$post_author] = array( 'contents' => array() );
				}

				$correios_packages[$post_author]['contents'][$item_id] = $item;
		  }

			$packages_shipping = array();
			foreach ( $correios_packages as $correios_package_id => $correios_package ) {
				$correios_package = array_merge( $package, $correios_package );
				$author_postcode = $this->get_author_zip_origin( $correios_package_id );
				$packages_shipping[$correios_package_id] = $this->correios_calculate( $correios_package, $author_postcode );
			}

			if ( 'yes' == $this->debug ) {
				$this->log->add( 'correios', 'Packages shipping pre-merge: '.print_r( $packages_shipping, true ) );
			}

			$shipping_values = array();
			$code_count = array();
      foreach ( $packages_shipping as $package_shipping ) {
				foreach ( $package_shipping as $code => $service ) {
					$code_count[$code] = (int)$code_count[$code] + 1;
					if ( !isset( $shipping_values[$code] ) ) {
						$shipping_values[$code] = $service;
					} else {
						$temp_obj = $shipping_values[$code];

						$temp_obj->Valor = number_format( WC_Correios_Connect::fix_currency_format( esc_attr( $service->Valor ) ) + WC_Correios_Connect::fix_currency_format( esc_attr( $temp_obj->Valor ) ), 2, ',', '.' );
						$temp_obj->PrazoEntrega = max( $temp_obj->PrazoEntrega, $service->PrazoEntrega );
						$temp_obj->ValorSemAdicionais = number_format( WC_Correios_Connect::fix_currency_format( esc_attr( $service->ValorSemAdicionais ) ) + WC_Correios_Connect::fix_currency_format( esc_attr( $temp_obj->ValorSemAdicionais ) ), 2, ',', '.' );
						$temp_obj->ValorMaoPropria = number_format( WC_Correios_Connect::fix_currency_format( esc_attr( $service->ValorMaoPropria ) ) + WC_Correios_Connect::fix_currency_format( esc_attr( $temp_obj->ValorMaoPropria ) ), 2, ',', '.' );
						$temp_obj->ValorAvisoRecebimento = number_format( WC_Correios_Connect::fix_currency_format( esc_attr( $service->ValorAvisoRecebimento ) ) + WC_Correios_Connect::fix_currency_format( esc_attr( $temp_obj->ValorAvisoRecebimento ) ), 2, ',', '.' );
						$temp_obj->ValorValorDeclarado = number_format( WC_Correios_Connect::fix_currency_format( esc_attr( $service->ValorValorDeclarado ) ) + WC_Correios_Connect::fix_currency_format( esc_attr( $temp_obj->ValorValorDeclarado ) ), 2, ',', '.' );
						$temp_obj->EntregaDomiciliar = $service->EntregaDomiciliar == 'N' ? $service->EntregaDomiciliar : $temp_obj->EntregaDomiciliar;
						$temp_obj->EntregaSabado = $service->EntregaSabado == 'N' ? $service->EntregaSabado : $temp_obj->EntregaSabado;
						if ( $temp_obj->Erro == '0' ) {
							$temp_obj->Erro = $service->Erro;
							$temp_obj->MsgErro = $service->MsgErro;
						};

						$shipping_values[$code] = $temp_obj;
					}
				}
			}

			$threshold = count( $packages_shipping );
			foreach ( $shipping_values as $code => $value ) {
				if ( $code_count[$code] != $threshold )
					unset($shipping_values[$code]);
			}

			if ( 'yes' == $this->debug ) {
				$this->log->add( 'correios', 'Packages shipping merged: '.print_r( $shipping_values, true ) );
			}
		} else {
			$shipping_values = $this->correios_calculate( $package, $this->zip_origin );
		}

		$fee_per_order = array();
		$fee_per_unit = array();
		$blocked_codes = array();
		foreach ( $package['contents'] as $item_id => $item ) {
			$product_id = isset( $item['variation_id'] ) ? $item['variation_id'] : $item['product_id'];

			$_blocked_codes_meta = get_post_meta( $product_id, '_wc_correios_block', true );
			$blocked_codes = empty( $_blocked_codes_meta ) ? $blocked_codes : array_merge( $blocked_codes, $_blocked_codes_meta );
			$_fee_per_order_meta = get_post_meta( $product_id, '_wc_correios_fee_per_order', true );
			$fee_per_order[$product_id] = empty( $_fee_per_order_meta ) ? 0 : $_fee_per_order_meta;
			$_fee_per_unit_meta = get_post_meta( $product_id, '_wc_correios_fee_per_unit', true );
			$fee_per_unit[$product_id] = empty( $_fee_per_unit_meta ) ? 0 : $_fee_per_unit_meta * $item['quantity'];
		}

		if ( 'yes' == $this->debug ) {
			$this->log->add( 'correios', 'blocked codes: '.print_r( $blocked_codes, true ) );
			$this->log->add( 'correios', 'fee per order: '.print_r( $fee_per_order, true ) );
			$this->log->add( 'correios', 'fee per unit : '.print_r( $fee_per_unit, true ) );
		}

    $total_product_fees = array_sum( $fee_per_order ) + array_sum( $fee_per_unit );


		foreach ( $shipping_values as $code => $shipping ) {
			if ( in_array( $code, $blocked_codes ) )
				unset( $shipping_values[$code] );
		}


		if ( ! empty( $shipping_values ) ) {
			foreach ( $shipping_values as $code => $shipping ) {
				if ( ! isset( $shipping->Erro ) ) {
					continue;
				}

				$name          = WC_Correios_Connect::get_service_name( $code );
				$error_number  = (string) $shipping->Erro;
				$error_message = WC_Correios_Error::get_message( $shipping->Erro );
				$errors[ $error_number ] = array(
					'error'   => $error_message,
					'number'  => $error_number
				);

				// Set the shipping rates.
				if ( in_array( $error_number, array( '0', '010' ) ) ) {
					$label = ( 'yes' == $this->display_date ) ? WC_Correios_Connect::estimating_delivery( $name, $shipping->PrazoEntrega, $this->additional_time ) : $name;
					$cost  = WC_Correios_Connect::fix_currency_format( esc_attr( $shipping->Valor ) );
					$fee   = $this->get_fee( str_replace( ',', '.', $this->fee ), $cost );

					array_push(
						$rates,
						array(
							'id'    => $name,
							'label' => $label,
							'cost'  => $cost + $fee + $total_product_fees,
						)
					);
				}
			}

			// Display correios errors.
			if ( ! empty( $errors ) ) {
				foreach ( $errors as $error ) {
					if ( '' != $error['error'] ) {
						$type = ( '010' == $error['number'] ) ? 'notice' : 'error';
						$message = '<strong>' . __( 'Correios', 'woocommerce-correios' ) . ':</strong> ' . esc_attr( $error['error'] );
						wc_add_notice( $message, $type );
					}
				}
			}

			$rates = apply_filters( 'woocommerce_correios_shipping_methods', $rates, $package );

			// Add rates.
			foreach ( $rates as $rate ) {
				$this->add_rate( $rate );
			}
		}
	}


	/**
	 * Gets a origin postcode from author.
	 *
	 * @param integer $author_id Author Id.
	 *
	 * @return string
	 */
	public function get_author_zip_origin( $author_id ) {
		$origin = '';

		if ( $author_id == -1 ) {
			$origin = $this->zip_origin;
		} else {
			$billing_postcode = get_user_meta( $author_id, 'billing_postcode', true );
			$shipping_postcode = get_user_meta( $author_id, 'shipping_postcode', true );

			if ( $this->multiorigin_order == 'billing' ) {
				if ( !empty( $billing_postcode ) )
					return $billing_postcode;
				else if ( !empty( $shipping_postcode ) )
					return $shipping_postcode;
			} else {
				if ( !empty( $shipping_postcode ) )
					return $shipping_postcode;
				else if ( !empty( $billing_postcode ) )
					return $billing_postcode;
			}
			$origin = $this->zip_origin;
		}

		return apply_filters( 'woocommerce_correios_author_origin', $origin, $author_id );
	}
}
