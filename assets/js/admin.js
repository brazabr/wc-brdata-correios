jQuery( document ).ready( function( $ ) {
	$( '#woocommerce_correios_display_date' ).on( 'change', function() {
		var field = $( this ).closest( 'tr' ).next( 'tr' );

		if ( $( this ).is( ':checked' ) ) {
			field.show();
		} else {
			field.hide();
		}
	}).change();

	$( '#woocommerce_correios_service_regletter' ).on( 'change', function() {
		var fields = $( '.form-table:eq(1) tr:eq(4), .form-table:eq(1) tr:eq(5)' );

		if ( $( this ).is( ':checked' ) ) {
			fields.show();
		} else {
			fields.hide();
		}
	}).change();

	$( '#woocommerce_correios_multiorigin' ).on( 'change', function() {
		var fields = $( '.form-table:eq(3) tr:eq(1)' );

		if ( $( this ).is( ':checked' ) ) {
			fields.show();
		} else {
			fields.hide();
		}
	}).change();

	$( '#woocommerce_correios_corporate_service' ).on( 'change', function() {
		var login    = $( '.form-table:eq(1) tr:eq(1)' ),
			password   = $( '.form-table:eq(1) tr:eq(2)' ),
			pacLarge   = $( '.form-table:eq(1) tr:eq(7)' ),
			eSedex     = $( '.form-table:eq(1) tr:eq(11)' );

		if ( 'corporate' === $( this ).val() ) {
			login.show();
			password.show();
			pacLarge.show();
			eSedex.show();
		} else {
			login.hide();
			password.hide();
			pacLarge.hide();
			eSedex.hide();
		}
	}).change();
});
