<?php
/**
 * Plugin Name: WooCommerce Correios (BRData Fork)
 * Plugin URI: https://bitbucket.org/brazabr/wc-brdata-correios
 * Description: Correios para WooCommerce
 * Author: Claudio Sanches, Thiago Benvenuto/BRData
 * Author URI: http://brdata.info/
 * Version: 2.3.0-alpha1
 * License: GPLv2 or later
 * Text Domain: woocommerce-correios
 * Domain Path: /languages/
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! class_exists( 'WC_Correios' ) ) :

/**
 * WooCommerce Correios main class.
 */
class WC_Correios {

	/**
	 * Plugin version.
	 *
	 * @var string
	 */
	const VERSION = '2.3.0';

	/**
	 * Instance of this class.
	 *
	 * @var object
	 */
	protected static $instance = null;

	/**
	 * Initialize the plugin public actions.
	 */
	private function __construct() {
		// Load plugin text domain
		add_action( 'init', array( $this, 'load_plugin_textdomain' ) );

		// Checks with WooCommerce is installed.
		if ( class_exists( 'WC_Shipping_Method' ) ) {
			$this->includes();

			if ( is_admin() ) {
				$this->admin_includes();
			}

			add_filter( 'woocommerce_shipping_methods', array( $this, 'add_method' ) );
			add_action( 'wp_ajax_wc_correios_simulator', array( 'WC_Correios_Product_Shipping_Simulator', 'ajax_simulator' ) );
			add_action( 'wp_ajax_nopriv_wc_correios_simulator', array( 'WC_Correios_Product_Shipping_Simulator', 'ajax_simulator' ) );
			add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'plugin_action_links' ) );

			add_action( 'woocommerce_product_options_shipping', array( $this, 'wc_correios_product_additional_fees' ) );
			add_action( 'woocommerce_process_product_meta', array( $this, 'wc_correios_product_save_fees' ) );

			add_action( 'woocommerce_product_after_variable_attributes', array( $this, 'wc_correios_product_additional_fees_variation' ), 100, 3 );
			add_action( 'woocommerce_save_product_variation', array( $this, 'wc_correios_product_save_fees_variation' ), 100, 2 );
		} else {
			add_action( 'admin_notices', array( $this, 'woocommerce_missing_notice' ) );
		}
	}

	/**
	 * Return an instance of this class.
	 *
	 * @return object A single instance of this class.
	 */
	public static function get_instance() {
		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Get templates path.
	 *
	 * @return string
	 */
	public static function get_templates_path() {
		return plugin_dir_path( __FILE__ ) . 'templates/';
	}

	/**
	 * Load the plugin text domain for translation.
	 */
	public function load_plugin_textdomain() {
		$locale = apply_filters( 'plugin_locale', get_locale(), 'woocommerce-correios' );

		load_textdomain( 'woocommerce-correios', trailingslashit( WP_LANG_DIR ) . 'woocommerce-correios/woocommerce-correios-' . $locale . '.mo' );
		load_plugin_textdomain( 'woocommerce-correios', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
	}

	/**
	 * Includes.
	 */
	private function includes() {
		include_once 'includes/class-wc-correios-error.php';
		include_once 'includes/class-wc-correios-package.php';
		include_once 'includes/class-wc-correios-connect.php';
		include_once 'includes/class-wc-correios-shipping.php';
		include_once 'includes/class-wc-correios-product-shipping-simulator.php';
		include_once 'includes/class-wc-correios-emails.php';
		include_once 'includes/class-wc-correios-tracking-history.php';
	}

	/**
	 * Admin includes.
	 */
	private function admin_includes() {
		include_once 'includes/admin/class-wc-correios-admin-orders.php';
	}

	/**
	 * Action links.
	 *
	 * @param  array $links
	 *
	 * @return array
	 */
	public function plugin_action_links( $links ) {
		$plugin_links = array();

		$plugin_links[] = '<a href="' . esc_url( admin_url( 'admin.php?page=wc-settings&tab=shipping&section=wc_correios_shipping' ) ) . '">' . __( 'Settings', 'woocommerce-correios' ) . '</a>';

		return array_merge( $plugin_links, $links );
	}

	/**
	 * Add the shipping method to WooCommerce.
	 *
	 * @param   array $methods WooCommerce payment methods.
	 *
	 * @return  array          Payment methods with Correios.
	 */
	public function add_method( $methods ) {
		$methods[] = 'WC_Correios_Shipping';

		return $methods;
	}

	/**
	 * WooCommerce fallback notice.
	 *
	 * @return  string
	 */
	public function woocommerce_missing_notice() {
		echo '<div class="error"><p>' . sprintf( __( 'WooCommerce Correios depends on the last version of %s to work!', 'woocommerce-correios' ), '<a href="http://wordpress.org/plugins/woocommerce/">' . __( 'WooCommerce', 'woocommerce-correios' ) . '</a>' ) . '</p></div>';
	}

	/**
	 * Add extra fields in shipping tab for product.
	 */
	public function wc_correios_product_additional_fees() {
		global $woocommerce, $post;

		?>
		</div>
	  <div class="options_group">
			<div style="margin-top: 10px;margin-left: 12px;"><strong><?php _e( 'Correios', 'woocommerce-correios' ); ?></strong></div>
		<?php

		woocommerce_wp_text_input(
			array(
				'id'          => '_wc_correios_fee_per_order',
				'data_type'   => 'price',
				'label'       => __( 'Extra fee per order', 'woocommerce-correios' ),
				'placeholder' => '0',
				'desc_tip'    => 'true',
				'description' => __( 'Additional fee to shipping when product is present in order.', 'woocommerce-correios' )
			)
		);

		woocommerce_wp_text_input(
			array(
				'id'          => '_wc_correios_fee_per_unit',
				'data_type'   => 'price',
				'label'       => __( 'Extra fee per unit', 'woocommerce-correios' ),
				'placeholder' => '0',
				'desc_tip'    => 'true',
				'description' => __( 'Additional fee to shipping per unit of product in order.', 'woocommerce-correios' )
			)
		);

		$shipping = new WC_Correios_Shipping;
		$services = $shipping->correios_services();
		$blocked = get_post_meta( $post->ID, '_wc_correios_block', true );
		$blocked = empty( $blocked ) ? array() : $blocked;
		?>
		<p class="form-field wc_correios_block">
			<label><?php _e( 'Allow shipping via', 'woocommerce-correios' ); ?></label><?php
			$i = 0;
			foreach( $services as $service) {?>
				<label style="margin-left: 7px;<?php echo $i > 0 ? "clear: left;" : ""; ?>">
					<input <?php checked( !in_array( $service, $blocked ) ); ?> type="checkbox" style="margin-top:4px;" class="checkbox" name="_wc_correios_block[]" value="<?php echo $service; ?>">
					<span class="description"><?php echo WC_Correios_Connect::get_service_name( $service ); ?><span>
	      </label><?php
				$i++;
			}?>
		</p>
		<?php
	}

	public function wc_correios_product_additional_fees_variation( $loop, $variation_data, $variation ) {
		?>
		<div class="">
			<p class="form-row form-row-full">
				<strong><?php _e( 'Correios', 'woocommerce-correios' ); ?></strong>
			</p>
		</div>
		<div class="">
			<?php
				woocommerce_wp_text_input(
					array(
						'id'          => '_wc_correios_fee_per_order_variation[' . $variation->ID . ']',
						'data_type'   => 'price',
						'label'       => __( 'Extra fee per order', 'woocommerce-correios' ),
						'placeholder' => '0',
						'desc_tip'    => 'true',
						'description' => __( 'Additional fee to shipping when product is present in order.', 'woocommerce-correios' ),
						'value'       => get_post_meta( $variation->ID, '_wc_correios_fee_per_order', true )
					)
				);
				woocommerce_wp_text_input(
					array(
						'id'          => '_wc_correios_fee_per_unit_variation[' . $variation->ID . ']',
						'data_type'   => 'price',
						'label'       => __( 'Extra fee per unit', 'woocommerce-correios' ),
						'placeholder' => '0',
						'desc_tip'    => 'true',
						'description' => __( 'Additional fee to shipping per unit of product in order.', 'woocommerce-correios' ),
						'value'       => get_post_meta( $variation->ID, '_wc_correios_fee_per_unit', true )
					)
				);?>
			</p>
		</div>
		<?php

		$shipping = new WC_Correios_Shipping;
		$services = $shipping->correios_services();
		$blocked = get_post_meta( $variation->ID, '_wc_correios_block', true );
		$blocked = empty( $blocked ) ? array() : $blocked;
		?>
		<p class="form-field wc_correios_block">
			<label><?php _e( 'Allow shipping via', 'woocommerce-correios' ); ?></label><?php
			$i = 0;
			foreach( $services as $service) {?>
				<label style="margin-left: 7px;<?php echo $i > 0 ? "clear: left;" : ""; ?>">
					<input <?php checked( !in_array( $service, $blocked ) ); ?> type="checkbox" style="margin-top:4px;" class="checkbox" name="_wc_correios_block_variation[<?php echo $variation->ID; ?>][]" value="<?php echo $service; ?>">
					<span class="description"><?php echo WC_Correios_Connect::get_service_name( $service ); ?><span>
	      </label><?php
				$i++;
			}?>
		</p>
		<?php
	}

	/**
	 * Save extra fields values from shipping tab for product.
	 */
	public function wc_correios_product_save_fees( $post_id ) {
		$fee_per_order = isset( $_POST['_wc_correios_fee_per_order'] ) ? $_POST['_wc_correios_fee_per_order'] : null;
		if( !empty( $fee_per_order ) ) {
			update_post_meta( $post_id, '_wc_correios_fee_per_order', esc_attr( $fee_per_order ) );
		} else {
			delete_post_meta( $post_id, '_wc_correios_fee_per_order' );
		}

		$fee_per_unit = isset( $_POST['_wc_correios_fee_per_unit'] ) ? $_POST['_wc_correios_fee_per_unit'] : null;
		if( !empty( $fee_per_unit ) ) {
			update_post_meta( $post_id, '_wc_correios_fee_per_unit', esc_attr( $fee_per_unit ) );
		} else {
			delete_post_meta( $post_id, '_wc_correios_fee_per_unit' );
		}

		$shipping = new WC_Correios_Shipping;
		$services = $shipping->correios_services();
		$checked = isset( $_POST['_wc_correios_block'] ) ? $_POST['_wc_correios_block'] : array();
		$blocked = array_diff( $services, $checked );
		if( !empty( $blocked ) ) {
			update_post_meta( $post_id, '_wc_correios_block', $blocked );
		} else {
			delete_post_meta( $post_id, '_wc_correios_block' );
		}
	}

	public function wc_correios_product_save_fees_variation( $post_id, $i ) {
		$fee_per_order = isset( $_POST['_wc_correios_fee_per_order_variation'][ $post_id ] ) ? $_POST['_wc_correios_fee_per_order_variation'][ $post_id ] : null;
		if( !empty( $fee_per_order ) ) {
			update_post_meta( $post_id, '_wc_correios_fee_per_order', esc_attr( $fee_per_order ) );
		} else {
			delete_post_meta( $post_id, '_wc_correios_fee_per_order' );
		}

		$fee_per_unit = isset( $_POST['_wc_correios_fee_per_unit_variation'][ $post_id ] ) ? $_POST['_wc_correios_fee_per_unit_variation'][ $post_id ] : null;
		if( !empty( $fee_per_unit ) ) {
			update_post_meta( $post_id, '_wc_correios_fee_per_unit', esc_attr( $fee_per_unit ) );
		} else {
			delete_post_meta( $post_id, '_wc_correios_fee_per_unit' );
		}

		$shipping = new WC_Correios_Shipping;
		$services = $shipping->correios_services();
		$checked = isset( $_POST['_wc_correios_block_variation'][ $post_id ] ) ? $_POST['_wc_correios_block_variation'][ $post_id ] : array();
		$blocked = array_diff( $services, $checked );
		if( !empty( $blocked ) ) {
			update_post_meta( $post_id, '_wc_correios_block', $blocked );
		} else {
			delete_post_meta( $post_id, '_wc_correios_block' );
		}
	}
}

add_action( 'plugins_loaded', array( 'WC_Correios', 'get_instance' ) );

endif;
